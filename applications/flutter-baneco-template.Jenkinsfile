def projectName = "viridian-flutter-baneco"
def versionNumber = "1.0.${BUILD_NUMBER}"
def cloudLink = "https://cloud.viridian.ltd/Public/"
def committerEmail = ""
def summary = ""
def slackFooter = '';

def flavor_bundle = false
def flavor_prod = false
def flavor_ac = false
def flavor_stage = false
def flavor_dev = false
def flavor_testing = false
def flavor_bot = false
def flavor_master = false

def slackMessage (projectNameVersion, summary, committerEmail, text) {
    
    def ciMessage = "*" + projectNameVersion + "* \n" + text + " \n" + summary + "\n_" + committerEmail + "_\n`SUCCESS` "
    slackSend(
            color: "good", 
            message: ciMessage,
            channel: "viridian-build-apk", 
            tokenCredentialId: "viridian-build-apk"
    );
}

node ('linux')  {

    def slackStage = '';
    def apkLink = '';
    def downloadLink = '';
    def jenkinschome = '/home/jenkins';
    
    println "* client: " + client 
    println "* flavors: " + flavors

    println flavors
    def flavorsList = "${flavors}".tokenize(",")
    println flavorsList

    def projectNameVersion = "viridian-" + client + ":" + versionNumber
    def artifactName        = client + "-" + versionNumber + ".apk"
    def artifactBundleName  = client + "-" + versionNumber + ".aab"
    def artifactAcName      = client + "-" + versionNumber + "-ac.apk"
    def artifactTestingName = client + "-" + versionNumber + "-testing.apk"
    def artifactDevName     = client + "-" + versionNumber + "-dev.apk"
    def artifactStageName   = client + "-" + versionNumber + "-stage.apk"
    def artifactBotName     = client + "-" + versionNumber + "-bot.apk"
    def artifactProdName    = client + "-" + versionNumber + "-prod.apk"
    def artifactMasterName  = client + "-" + versionNumber + "-master.apk"
    
    if (flavorsList.contains("bundle")) {
        flavor_bundle = true;
    }
    
    if (flavorsList.contains("prod")) {
        flavor_prod = true;
    }
    
    if (flavorsList.contains("ac")) {
        flavor_ac = true;
    }
    
    if (flavorsList.contains("stage")) {
        flavor_stage = true;
    }
    
    if (flavorsList.contains("dev")) {
        flavor_dev = true;
    }

    if (flavorsList.contains("testing")) {
        flavor_testing = true;
    }

    if (flavorsList.contains("bot")) {
        flavor_bot = true;
    }

    if (flavorsList.contains("master")) {
        flavor_master = true;
    }

    echo flavor_prod.toString();
    echo flavor_bundle.toString();
    echo flavor_ac.toString();
    echo flavor_stage.toString();
    echo flavor_dev.toString();
    echo flavor_testing.toString();
    echo flavor_bot.toString();
    echo flavor_master.toString();
    
    try {
        stage('scm') {
            slackStage = 'start';
            dir('build') {
                git branch: 'master',
                credentialsId: 'gitlabintegration',
                url: 'http://gitlab.viridian.ltd/vdb/mb/viridian-flutter-baneco.git'
            }

            dir('ci') {
                git url: 'http://gitlab.viridian.ltd/vdb/ci/viridian-flutter-baneco.git',
                credentialsId: 'gitlabintegration',
                branch: 'master'
            }

            dir("build") {
                sh('/usr/bin/gradle changeClient -Penv=dev -Pclient=' + client)
            }

            dir("build") {
                committerEmail = sh (
                    script: 'git log -1 --no-merges --pretty=format:\'%an\' ',
                    returnStdout: true
                ).trim()

                summary = sh (
                     script: 'git log -1 --no-merges --pretty=format:\'%s\' ',
                     returnStdout: true
                ).trim()
            }
            echo summary
            echo committerEmail
            currentBuild = {};
            currentBuild.result = "SUCCESS";
        }

        stage('clean') {
            dir("build") {
                //change files before build
                sh "cp pubspec.jenkins.yaml pubspec.yaml"
                sh 'sed -i "s/.999/.${BUILD_NUMBER}/g"  pubspec.yaml'

                sh "cp android/app/build.s7.gradle android/app/build.gradle"
                sh 'sed -i "s/999/${BUILD_NUMBER}/g"  android/app/build.gradle'

                sh "cp android/key.s7.properties android/key.properties"

                //clean
                sh "pwd"
                sh "flutter packages get"
                sh "flutter clean"
            }
        }
        
        stage('prod') {
            parallel (
                "build prod": {
                    if (flavor_prod == true) {
                        echo "building " + artifactProdName
                        dir("build") {
                            sh "cp android/app/firebase/baneco/google-services.json android/app/google-services.json"
                            sh "flutter build apk --flavor prod --verbose --target lib/main-prod.dart"
                            sh "mv build/app/outputs/apk/prod/release/app-prod-release.apk ~/tmp/" + artifactProdName
                        }
                    }
                }
            )
        }
        
        stage('bundle') {
            parallel (
                "cloud prod": {
                    if (flavor_prod == true) {
                        echo "entering cloud prod"
                        dir('ci') {
                            sh "bash deploy-cloud.sh ${versionNumber} 'prod' "
                            slackMessage (projectNameVersion, summary, committerEmail, " <" + cloudLink + artifactProdName + "| " + artifactProdName + ">")
                        }
                    }
                },
                "build bundle": {
                    if (flavor_bundle == true) {
                        echo "building " + artifactBundleName
                        dir("build") {
                            sh "cp android/app/firebase/baneco/google-services.json android/app/google-services.json"
                            sh "flutter build appbundle --target-platform android-arm,android-arm64 --flavor prod --verbose --target lib/main-prod.dart  || true"
                            sleep 5
                            sh "ls -lhs  build/app/outputs/bundle/prodRelease"
                            sh "mv build/app/outputs/bundle/prodRelease/app-prod-release.aab	~/tmp/" + artifactBundleName
                        }
                    }
                }
            )
        }
        
        stage('stage') {
            parallel (
                "cloud bundle": {
                    echo "entering cloud bundle"
                    if (flavor_bundle == true) {
                        dir('ci') {
                            sh "ls -lhs *"
                            sh "bash deploy-cloud-bundle.sh ${versionNumber} 'bundle' "
                            slackMessage (projectNameVersion, summary, committerEmail, " <" + cloudLink + artifactBundleName + "| " + artifactBundleName + ">")
                        }
                    }
                },
                "build stage": {
                    if (flavor_stage == true) {
                        echo "building " + artifactStageName
                        dir("build") {
                            sh "flutter build apk --flavor stage --verbose --target lib/main-stage.dart"
                            sh "mv build/app/outputs/apk/stage/release/app-stage-release.apk ~/tmp/" + artifactStageName
                        }
                    }
                }
            )
        }
        
        stage('dev') {
            parallel (
                "cloud stage": {
                    if (flavor_stage == true) {
                        dir('ci') {
                            sh "bash deploy-cloud.sh ${versionNumber} 'stage' "
                            slackMessage (projectNameVersion, summary, committerEmail, " <" + cloudLink + artifactStageName + "| " + artifactStageName + ">")
                        }
                    }
                },
                "build dev": {
                    if (flavor_dev == true) {
                        echo "building " + artifactDevName
                        dir("build") {
                            sh "flutter build apk --flavor dev --verbose --target lib/main-dev.dart"
                            sh "mv build/app/outputs/apk/dev/release/app-dev-release.apk ~/tmp/" + artifactDevName
                        }
                    }
                }
            )
        }

        stage('testing') {
            parallel (
                "cloud dev": {
                    if (flavor_dev == true) {
                        dir('ci') {
                            sh "bash deploy-cloud.sh ${versionNumber} 'dev' "
                            slackMessage (projectNameVersion, summary, committerEmail, " <" + cloudLink + artifactDevName + "| " + artifactDevName + ">")
                        }
                    }
                },
                "build testing": {
                    if (flavor_testing == true) {
                        echo "building " + artifactTestingName
                        dir("build") {
                            sh "flutter build apk --flavor automated --verbose --target lib/main-testing.dart"
                            sh "mv build/app/outputs/apk/automated/release/app-automated-release.apk ~/tmp/" + artifactTestingName
                        }
                    }
                }
            )
        }

        stage('bot') {
            parallel (
                "cloud testing": {
                    if (flavor_testing == true) {
                        dir('ci') {
                            sh "bash deploy-cloud.sh ${versionNumber} 'testing' "
                            slackMessage (projectNameVersion, summary, committerEmail, " <" + cloudLink + artifactTestingName + "| " + artifactTestingName + ">")
                        }
                    }
                },
                "build bot": {
                    if (flavor_bot == true) {
                        echo "building " + artifactName
                        dir("build") {
                            sh "flutter build apk --flavor bot --verbose --target lib/main-bot.dart"
                            sh "mv build/app/outputs/apk/bot/release/app-bot-release.apk ~/tmp/" + artifactBotName
                        }
                    }
                }
            )
        }

        stage('master') {
            parallel (
                "cloud bot": {
                    if (flavor_bot == true) {
                        dir('ci') {
                            sh "bash deploy-cloud.sh ${versionNumber} 'bot' "
                            slackMessage (projectNameVersion, summary, committerEmail, " <" + cloudLink + artifactBotName + "| " + artifactBotName + ">")
                        }
                    }
                },
                "build master": {
                    if (flavor_master == true) {
                        echo "building " + artifactName
                        dir("build") {
                            sh "flutter build apk --flavor master --verbose --target lib/main-master.dart"
                            sh "mv build/app/outputs/apk/master/release/app-master-release.apk ~/tmp/" + artifactMasterName
                        }
                    }
                }
            )
        }

        stage("clean up") {
            if (flavor_master == true) {
                dir('ci') {
                    sh "bash deploy-cloud.sh ${versionNumber} 'master' "
                    slackMessage (projectNameVersion, summary, committerEmail, " <" + cloudLink + artifactMasterName + "| " + artifactMasterName + ">")
                }
            }
            dir("build") {
                sh "rm  ~/tmp/*.apk || true"
            }
        }

    } catch (Exception e) {
        echo "exception thrown"
        //echo e
        currentBuild.result = "FAILED"
        slackFooter = "\n`${currentBuild.result}`";
        ciMessage = "*" + projectNameVersion + "* error in Jenkins \n" + summary + "\n_" + committerEmail + "_" + slackFooter
        slackSend (
            color: "danger", 
            message: ciMessage,
            channel: "viridian-build-apk", 
            tokenCredentialId: "viridian-build-apk"
        )
    }
  
}

