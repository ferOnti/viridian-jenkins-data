def projectName = "viridian-frontend-baneco"
def versionNumber = "0.2.${BUILD_NUMBER}"
def projectNameVersion = "${projectName}:${versionNumber}"
def dockerImage = "${DOCKER_REGISTRY}/${projectNameVersion}"
def dockerTag = '{\\"tag\\":\\"' + versionNumber + '\\"}'
def committerEmail = ""
def summary = ""

//node ('fernando-ubuntu') {
node ('linux') {

    try{
        stage('Scm') {
            dir("build") {
                git branch: 'dev',
                credentialsId: 'gitlab_jenkins',
                url: "http://gitlab.viridian.ltd/vdb/fe/${projectName}.git"
            }
            sh('du -hcs *')

            dir("build") {
                committerEmail = sh (
                    script: 'git log -1 --no-merges --pretty=format:\'%an\' ',
                    returnStdout: true
                ).trim()

                summary = sh (
                    script: 'git log -1 --no-merges --pretty=format:\'%s\' ',
                    returnStdout: true
                ).trim()
            }
        }

        stage('Install npm deps') {
            echo "installing npm deps ${projectNameVersion}"
            dir("build") {
                sh "npm i"
            }
        }

        stage('Linting') {
            echo "linting code ${projectNameVersion}"
            dir("build") {
                sh "ng lint"
            }
        }

        stage('Build dev') {
            echo "building ${projectNameVersion}"
            dir("build") {
                sh "sed -i 's/0.1.9999/${versionNumber}/g' src/environments/environment.dev.ts"
                sh "npm run build:dev"
            }
        }

        stage('Docker dev') {
            echo "dockerizing ${projectNameVersion}"
            dir("build") {
                docker.withRegistry("https://${DOCKER_REGISTRY}", 'docker-credentials') {
                    def customImage = docker.build("${dockerImage}", "--build-arg ENV=dev .")
                    customImage.push()
                }
            }
        }
        stage("Deploy dev") {
            dir("build") {
                try {
                    sh "ansible-playbook dev.playbook.yml --extra-vars ${dockerTag}"
                    currentBuild.result = "SUCCESS"
                } catch (Exception e){
                    currentBuild.result = "UNSTABLE"
                }
            }
        }


        stage('Build Bankio') {
            echo "building ${projectNameVersion}"
            dir("build") {
                sh "sed -i 's/0.1.9999/${versionNumber}/g' src/environments/environment.bankio.dev.ts"
                sh "npm run build:bankio-dev"
            }
        }
        stage('Docker bankio-dev') {
            echo "dockerizing ${projectNameVersion}"
            dir("build") {
                docker.withRegistry("https://${DOCKER_REGISTRY}", 'docker-credentials') {
                    def customImage = docker.build("${dockerImage}", "--build-arg ENV=bankio-dev .")
                    customImage.push()
                }
            }
        }
        stage("Deploy Bankio") {
            dir("build") {
                try {
                    sh "ansible-playbook master.playbook.yml --extra-vars ${dockerTag}"
                    currentBuild.result = "SUCCESS"
                } catch (Exception e){
                    currentBuild.result = "UNSTABLE"
                }
            }
        }
    } catch (Exception e) {
        currentBuild.result = "FAILED"
    } finally {
        dir("build"){
            def slackFooter = "`${currentBuild.result}`";
            def colorCode = 'good'
            def ciMessage = ""
            def url = "https://dev.viridian.ltd:9000"

            if (currentBuild.result == 'SUCCESS') {
                ciMessage = "*" + projectNameVersion + "*" + " \n" + url + " \n" + summary + "\n_" + committerEmail + "_\n" + slackFooter
            } else if (currentBuild.result == 'UNSTABLE') {
                colorCode = 'warning'
                ciMessage = "*" + projectNameVersion + "* can't be deployed  \n" + summary + "\n_" + committerEmail + "_\n" + slackFooter
            } else {
                colorCode = 'danger'
                ciMessage = "*" + projectNameVersion + "* error in Jenkins \n" + summary + "\n_" + committerEmail + "_\n" + slackFooter
            }

            slackSend (color: colorCode, message: ciMessage)
        }
    }
}
