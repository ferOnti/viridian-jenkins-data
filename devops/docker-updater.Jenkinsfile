@Library('viridian') _

node('linux') {
    def slackStage = '';
    def commitMessage = "chore: update docker base image"

    try {
        slack.init()
        slack.setTitle("docker-updater")
            
            stage('MANAGER-API') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/manager/Viridian.Manager.Api.git"

            dir("Viridian.Manager.Api/Viridian.Manager.Api") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Manager.Api") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('MANAGER-HOSTED') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/manager/Viridian.Manager.Hosted.git"

            dir("Viridian.Manager.Hosted/Viridian.Manager.Hosted") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Manager.Hosted") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }
        stage('ACCOUNTS-API') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/acc/Viridian.Accounts.Api.git"

            dir("Viridian.Accounts.Api/Viridian.Accounts.Api") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Accounts.Api") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('ACCOUNTS-HOSTED') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/acc/Viridian.Accounts.Hosted.git"

            dir("Viridian.Accounts.Hosted/Viridian.Accounts.Hosted") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Accounts.Hosted") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('SECURITY-API') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/sec/Viridian.Security.Api.git"

            dir("Viridian.Security.Api/Viridian.Security.Api") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Security.Api") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('SECURITY-HOSTED') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/sec/Viridian.Security.Hosted.git"

            dir("Viridian.Security.Hosted/Viridian.Security.Hosted") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Security.Hosted") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('BENEFICIARIES-API') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/beneficiaries/Viridian.Beneficiaries.Api.git"

            dir("Viridian.Beneficiaries.Api/Viridian.Beneficiaries.Api") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Beneficiaries.Api") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('BENEFICIARIES-HOSTED') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/beneficiaries/Viridian.Beneficiaries.Hosted.git"

            dir("Viridian.Beneficiaries.Hosted/Viridian.Beneficiaries.Hosted") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Beneficiaries.Hosted") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('SIMPLE-API') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/simple/Viridian.Simple.Api.git"

            dir("Viridian.Simple.Api/Viridian.Simple.Api") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Simple.Api") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('SERVICEPAYMENTS-API') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/service-payments/Viridian.ServicePayments.Api.git"

            dir("Viridian.ServicePayments.Api/Viridian.ServicePayments.Api") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.ServicePayments.Api") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('TRANSACTIONS-API') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/transactions/Viridian.Transactions.Api.git"

            dir("Viridian.Transactions.Api/Viridian.Transactions.Api") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Transactions.Api") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('TRANSACTIONS-HOSTED') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/transactions/Viridian.Transactions.Hosted.git"

            dir("Viridian.Transactions.Hosted/Viridian.Transactions.Hosted") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Transactions.Hosted") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('UTILITIES-API') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/utilities/Viridian.Utilities.Api.git"

            dir("Viridian.Utilities.Api/Viridian.Utilities.Api") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Utilities.Api") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('UTILITIES-HOSTED') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/utilities/Viridian.Utilities.Hosted.git"

            dir("Viridian.Utilities.Hosted/Viridian.Utilities.Hosted") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Utilities.Hosted") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('NOTIFICATIONS-HOSTED') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/notifications/Viridian.Notifications.Hosted.git"

            dir("Viridian.Notifications.Hosted/Viridian.Notifications.Hosted") {
                sh "sed -i \"1s/:.*/:${env.docker_netcore_version}/\" Dockerfile"
            }
            dir("Viridian.Notifications.Hosted") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                } else {
                    println("nothing to update")
                }
            }
        }

        currentBuild.result = "SUCCESS"
        slack.setSubTitle("All services updated, wait for individual builds")
    } catch(Exception ex) {
        currentBuild.result = "FAILED"
        slack.setSubTitle("CI failed in stage: ${slackStage}")
    } finally {
        slack.message()
        deleteDir()
    }
}
