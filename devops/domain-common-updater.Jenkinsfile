@Library('viridian') _

node('linux') {
    def slackStage = '';
    def commonVersion = params.common_version
    def commitMessage = "update common package to ${commonVersion}"

    try {
        slack.init()
        slack.setTitle("domain-common-updater")
    
        sh "dotnet nuget locals http-cache --clear"
        
        stage('MANAGER') {
            slackStage = env.STAGE_NAME
            sh "git clone git@gitlab.viridian.ltd:vdb/manager/Viridian.Manager.Common.git"

            dir("Viridian.Manager.Common/Viridian.Manager.Common") {
                sh "dotnet add package Viridian.Common -v ${commonVersion}"
            }
            dir("Viridian.Manager.Common") {
               def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                    sleep 2
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('SECURITY') {
            slackStage = env.STAGE_NAME

            sh "git clone git@gitlab.viridian.ltd:vdb/sec/Viridian.Security.Common.git"

            dir("Viridian.Security.Common/Viridian.Security.Common") {
                sh "dotnet add package Viridian.Common -v ${commonVersion}"
            }
            dir("Viridian.Security.Common") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                    sleep 2
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('ACCOUNTS') {
            slackStage = env.STAGE_NAME
            sh "git clone git@gitlab.viridian.ltd:vdb/acc/Viridian.Accounts.Common.git"

            dir("Viridian.Accounts.Common/Viridian.Accounts.Common") {
                sh "dotnet add package Viridian.Common -v ${commonVersion}"
            }
            dir("Viridian.Accounts.Common") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                    sleep 2
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('BENEFICIARIES') {
            slackStage = env.STAGE_NAME
            sh "git clone git@gitlab.viridian.ltd:vdb/beneficiaries/Viridian.Beneficiaries.Common.git"

            dir("Viridian.Beneficiaries.Common/Viridian.Beneficiaries.Common") {
                sh "dotnet add package Viridian.Common -v ${commonVersion}"
            }
            dir("Viridian.Beneficiaries.Common") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                    sleep 2
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('SIMPLE') {
            slackStage = env.STAGE_NAME
            sh "git clone git@gitlab.viridian.ltd:vdb/simple/Viridian.Simple.Common.git"

            dir("Viridian.Simple.Common/Viridian.Simple.Common") {
                sh "dotnet add package Viridian.Common -v ${commonVersion}"
            }
            dir("Viridian.Simple.Common") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                    sleep 2
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('SERVICEPAYMENTS') {
            slackStage = env.STAGE_NAME
            sh "git clone git@gitlab.viridian.ltd:vdb/service-payments/Viridian.ServicePayments.Common.git"

            dir("Viridian.ServicePayments.Common/Viridian.ServicePayments.Common") {
                sh "dotnet add package Viridian.Common -v ${commonVersion}"
            }
            dir("Viridian.ServicePayments.Common") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                    sleep 2
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('TRANSACTIONS') {
            slackStage = env.STAGE_NAME
            sh "git clone git@gitlab.viridian.ltd:vdb/transactions/Viridian.Transactions.Common.git"

            dir("Viridian.Transactions.Common/Viridian.Transactions.Common") {
                sh "dotnet add package Viridian.Common -v ${commonVersion}"
            }
            dir("Viridian.Transactions.Common") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                    sleep 2
                } else {
                    println("nothing to update")
                }
            }
        }
        
        stage('UTILITIES') {
            slackStage = env.STAGE_NAME
            sh "git clone git@gitlab.viridian.ltd:vdb/utilities/Viridian.Utilities.Common.git"

            dir("Viridian.Utilities.Common/Viridian.Utilities.Common") {
                sh "dotnet add package Viridian.Common -v ${commonVersion}"
            }
            dir("Viridian.Utilities.Common") {
                def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                    sleep 2
                } else {
                    println("nothing to update")
                }
            }
        }
   
        stage('NOTIFICATIONS') {
            slackStage = env.STAGE_NAME
            sh "git clone git@gitlab.viridian.ltd:vdb/notifications/Viridian.Notifications.Common.git"

            dir("Viridian.Notifications.Common/Viridian.Notifications.Common") {
                sh "dotnet add package Viridian.Common -v ${commonVersion}"
            }
            dir("Viridian.Notifications.Common") {
               def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                    sleep 2
                } else {
                    println("nothing to update")
                }
            }
        }

        stage('BANECO') {
            slackStage = env.STAGE_NAME
            sh "git clone git@gitlab.viridian.ltd:vdb/common/Viridian.Common.Baneco.git"

            dir("Viridian.Common.Baneco/Viridian.Common.Baneco") {
                sh "dotnet add package Viridian.Common -v ${commonVersion}"
            }
            dir("Viridian.Common.Baneco") {
               def commitRequired = sh (
                    script: "git status --porcelain -uno | cut -c4-",
                    returnStdout: true
                ).trim()

                if (commitRequired != '') {
                    println(commitRequired)
                    sh "git add ${commitRequired}"
                    sh "git commit -m '${commitMessage}'"
                    sh "git push"
                    sleep 2
                } else {
                    println("nothing to update")
                }
            }
        }
        currentBuild.result = "SUCCESS"
        slack.setSubTitle("All domain.common packages updated, wait for individual builds")
    } catch(Exception ex) {
        currentBuild.result = "FAILED"
        slack.setSubTitle("CI failed in stage: ${slackStage}")
    } finally {
        sh "rm -rf Viridian.*"
        slack.message()
        deleteDir()
    }
}
