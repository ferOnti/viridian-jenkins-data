@Library('viridian') _

node('linux') {

    def projects = []
    try {
        slack.init('viridian-tests')
        slack.setTitle("test-executor")

        def rawProjectsPassed = []
        def rawProjectsFailed = []

        stage('GET PROJECTS') {
            def rawProjects = sql.getProjectForTestExecution()

            rawProjects.each {item ->
                rawProjectsPassed << ['id':item.id, 'name':item.name]
                def projectFound = projects.find{ it.name == item.name }
                if(projectFound ==  null) {
                    def project = ['id':item.id, 'name':item.name, 'version':item.version, 'date_created':item.date_created, 'docker_image':item.docker_image, 'container_name':item.container_name, 'port':item.port, 'playbook_type':item.playbook_type]
                    projects << project
                } else {
                    if(item.date_created > projectFound.date_created) {
                        projects.removeAll{ it.name == projectFound.name}

                        def project = ['id':item.id, 'name':item.name, 'version':item.version, 'date_created':item.date_created, 'docker_image':item.docker_image, 'container_name':item.container_name, 'port':item.port, 'playbook_type':item.playbook_type]
                        projects << project
                    }
                }
            }
        }

        if(projects.size() > 0) {
            stage('GET FILES') {
                def stepsForParallel = [:]
                projects.each { project ->
                    stepsForParallel["getting files for ${project.container_name}"] = { ->
                        dir(project.name) {
                            def group = project.name == 'green_bank' ? 'gb' : 'vdb'
                            git branch: 'master', url: "git@gitlab.viridian.ltd:${group}/ci/${project.name}.git"
                        }
                    }
                }
                parallel stepsForParallel
            }

            stage('DEPLOY TESTING') {
                def stepsForParallel = [:]
                projects.each { project ->
                    stepsForParallel["deploying ${project.container_name}"] = { ->
                        ansible.deploy('testing', project.docker_image, project.version, project.container_name, project.port, project.playbook_type)
                    }
                }
                parallel stepsForParallel
            }

            stage('TESTS') {
                projects.each { project ->

                    switch(project.name) {
                        case 'Viridian.Manager.Api':
                            dir(project.name) {
                                sql.executeSqlScript("truncate-seed.sql")
                                sql.executeSqlScript("create-postman-users-for-account-tests.sql")
                                test.healthCheck(project.port)

                                project << test.executeCollections(["Manager.json", "Manager.Accounts.json"])
                            }
                            break
                        case 'Viridian.Security.Api':
                            dir(project.name) {
                                sql.executeSqlScript("create-customer-user-devices.sql")
                                test.healthCheck(project.port)
                                project << test.executeCollections(["Security.json"])
                            }
                            break
                        case 'Viridian.Accounts.Api':
                            dir(project.name) {
                                sql.executeSqlScript("create-postman-users.sql")
                                test.healthCheck(project.port)

                                project << test.executeCollections(["Accounts80transfers.json", "Accounts.json", "AccountsUserDailyLimit-FirstTimeBs1000.json", "AccountsTransfersAML.json"])
                            }
                            break
                        case 'Viridian.Beneficiaries.Api':
                            dir(project.name) {
                                test.healthCheck(project.port)
                                project << test.executeCollections(["Beneficiaries.json"])
                            }
                            break
                        case 'Viridian.Transactions.Api':
                            dir(project.name) {
                                sql.executeSqlScript("create-postman-users.sql")
                                test.healthCheck(project.port)
                                project << test.executeCollections(["Transactions.json"])
                            }
                            break
                        case 'Viridian.ServicePayments.Api':
                            dir(project.name) {
                                sql.executeSqlScript("create-postman-companies.sql")
                                test.healthCheck(project.port)
                                project << test.executeCollections(["ServicePayments.Admin.json"])
                            }
                            break
                        case 'Viridian.Simple.Api':
                            dir(project.name) {
                                sql.executeSqlScript("create-postman-users.sql")
                                test.healthCheck(project.port)

                                project << test.executeCollections(["Simple.Charges.json", "Simple.Payments.json"])
                            }
                            break
                        case 'Viridian.Simple.CertsManager':
                            dir(project.name) {
                                test.healthCheck(project.port)
                                project << test.executeCollections(["Simple.CertsManager.json"])
                            }
                            break
                        case 'Viridian.Utilities.Api':
                            dir(project.name) {
                                test.healthCheck(project.port)

                                project << test.executeCollections(["Utilities.json", "UtilitiesFormManagerAndTransactionControl.json"])
                                project << test.executeCollections(["Utilities.json"])
                            }
                            break
                        case 'Viridian.Notifications.Api':
                            dir(project.name) {
                                sql.executeSqlScript("create.sql")
                                test.healthCheck(project.port)

                                project << test.executeCollections(["Notifications.json"])
                            }
                            break
                        case 'server-config':
                            dir(project.name) {
                                sleep 50
                                project << test.executeCollections(["viridian.config.server.json"])
                            }
                            break
                        case 'green_bank':
                            dir(project.name) {
                                sql.executeSqlScript("updateInvoice.sql")

                                project << ['total':0,'total_pass':0,'total_fail':0]
                            }
                            break
                    }
                }

                def message = "```\n"

                projects.each { project ->
                    message += " * ${project.container_name}:${project.version}\n"
                    message += "\tTotal:".padRight(13) + "${project.total.toString().padLeft(5)}\n"
                    message += "\tTotal pass:".padRight(13) + "${project.total_pass.toString().padLeft(5)}\n"
                    message += "\tTotal fail:".padRight(13) + "${project.total_fail.toString().padLeft(5)}\n"

                    if(project.total_fail > 0) {
                        rawProjectsFailed.addAll(rawProjectsPassed.findAll { it.name == project.name })
                        rawProjectsPassed.removeAll { it.name == project.name}
                    }
                }

                message += "```"
                slack.setSubTitle(message)
            }

            stage('UPDATE QUEUE') {
                sql.updateTestQueuePassed(rawProjectsPassed)
                sql.updateTestQueueFailed(rawProjectsFailed)
            }
        } else {
            println 'No projects for test execution'
        }
    } catch(Exception ex) {
        println ex
        currentBuild.result = 'FAILURE'
        slack.setSubTitle("There was an error in test-executor")
    } finally {
        if(projects.size() > 0) {
            slack.message()
        }
        deleteDir()
    }
}
