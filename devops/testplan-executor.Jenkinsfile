@Library('viridian') _

node('linux') {

    def projects = []
    try {
        slack.init('viridian-tests')
        slack.setTitle("testplan-executor")

        def rawProjectsPassed = []
        def rawProjectsFailed = []

        stage('GET PROJECTS') {
            def rawProjects = sql.getTestableProjects()
            rawProjects.each {item ->
                def projectItem = ['id':item.id, 'name':item.name]
                projects << projectItem
            }
        }

        stage('GET FILES') {
            def parallelSteps = [:]
            def delay = 1
            projects.each { project ->
                project.delay = delay
                parallelSteps["parallell ${project.name}"] = { ->
                    dir (project.name) {
                        sleep(time:project.delay, unit:"SECONDS")
                        git branch: 'master', url: "git@gitlab.viridian.ltd:vdb/ci/${project.name}.git"
                    }
                }
                delay ++
            }
            parallel parallelSteps
        }

        stage('TESTS') {
            def results = []
            projects.each { project ->
                println project.name
                dir (project.name) {
                    if (fileExists("full.testplan") ) {
                        def testplanFile = readFile "full.testplan"
                        def lines = testplanFile.readLines()
                        def projectResults = [project:project.name, total:0, total_pass:0, total_fail:0]
                        lines.each { line ->
                            if (line.trim() != "") {
                                def lineSplit = line.split(" ")
                                if (lineSplit[0].equalsIgnoreCase("sql")) {
                                    println line
                                    try {
                                        sql.executeSqlScript(lineSplit[1])
                                    } catch(Exception ex) {
                                        println ex
                                    }
                                }
                                if (lineSplit[0].equalsIgnoreCase("postman")) {
                                    println line
                                    def executeResults = []
                                    executeResults << test.executeCollections([lineSplit[1]])
                                    println executeResults
                                    projectResults.total += executeResults.total
                                    projectResults.total_pass += executeResults.total_pass
                                    projectResults.total_fail += executeResults.total_fail
                                }
                            }
                        }
                        println projectResults
                        results << projectResults
                    }
                }
            }
            println results

            def message = "```\n"
            message += "Project                          total   pass   fail \n"

            def sumTotal = 0, sumPass = 0, sumFail = 0
            results.each { item ->
                message += "${item.project.padRight(30)} "
                message += "${item.total.toString().padLeft(7)}"
                message += "${item.total_pass.toString().padLeft(7)}"
                message += "${item.total_fail.toString().padLeft(7)}\n"

                sumTotal += item.total
                sumPass  += item.total_pass
                sumFail  += item.total_fail
            }

            message += "Total                          ${sumTotal.toString().padLeft(7)}${sumPass.toString().padLeft(7)}${sumFail.toString().padLeft(7)}\n"

            message += "```"
            slack.setSubTitle(message)

            println "* End TESTS"
        }

    } catch(Exception ex) {
        println ex
        currentBuild.result = 'FAILURE'
        slack.setSubTitle("There was an error in testplan-executor")
    } finally {
        slack.message()
    }
}

