package library;

import groovy.sql.Sql;

class SqlHelper {

    private final static String DB_HOST = "informix.viridian.ltd";
    private final static String DB_PORT = "3306";
    private final static String DB_NAME = "jenkins_data";
    private final static String DB_USER = "root";
    private final static String DB_PASS = "sesamo";

    public static List<String> getVersions(projectName){
        List<String> versions = [];

        def sql = getConnection();
        def result = sql.rows("SELECT version FROM project_versions WHERE name ='${projectName}' ORDER BY in_production DESC, date DESC");

        result.each {row -> 
            versions << row.version
        }

        sql.close();

        return versions;
    }

    public static String getVersionInfo(projectName, projectVersion){
        String info = [];

        def sql = getConnection();
        def result = sql.firstRow("SELECT * FROM project_versions WHERE name ='${projectName}' and version ='${projectVersion}'")


        sql.close();
        return "<ul><li>Commit: ${result.commit_hash}</li><li>Build date: ${result.date.toString()}</li><li>In Prod: ${result.in_production}</li></ul>"
    }

    public static List<String> getProductionVersions(projectName){
        List<String> versions = [];

        def sql = getConnection();
        def result = sql.rows("SELECT version FROM project_versions WHERE name ='${projectName}' and in_production = TRUE ORDER BY date DESC");

        result.each {row -> 
            versions << row.version
        }

        sql.close();

        return versions;
    }

    public static List<String> getPackageVersions(projectName){
        List<String> versions = [];

        def sql = getConnection();
        def result = sql.rows("SELECT version FROM project_versions WHERE name ='${projectName}' and (for_package = true or in_production = true) order by for_package DESC, in_production DESC, date DESC")

        result.each {row -> 
            versions << row.version
        }

        sql.close();

        return versions;
    }

    public static List<String> getDeployableProjects(){
        List<String> projects = [];

        def sql = getConnection();
        def result = sql.rows("select name from project_versions where is_dependency = FALSE group by name")

        result.each {row -> 
            projects << row.name
        }

        sql.close();

        return projects;
    }

    public static List<String> getProjects(){
        List<String> projects = [];

        def sql = getConnection();
        def result = sql.rows("SELECT name FROM projects ORDER by name");

        result.each {row -> 
            projects << row.name
        }

        sql.close();

        return projects;
    }

    public static List<String> getInProgressPackages(){
        List<String> packages = [];

        def sql = getConnection();
        def result = sql.rows("select version from packages where status = 'IN_PROGRESS'  order by date desc")

        result.each {row -> 
            packages << row.version
        }

        sql.close();

        return packages;
    }

    public static String getPackageInfo(packageVersion){
        String info = [];

        def sql = getConnection();
        def result = sql.firstRow("SELECT * FROM packages WHERE version ='${packageVersion}'")

        sql.close();
        return "<ul><li>Creation date: ${result.date.toString()}</li><li>Detail: ${result.detail}</li></ul>"
    }

    public static List<String> getEnvironments(){
        List<String> envs = [];

        def sql = getConnection();
        def result = sql.rows("SELECT env FROM environments WHERE enabled = TRUE")

        result.each {row -> 
            envs << row.env
        }

        sql.close();

        return envs;
    }

    private static Sql getConnection() {
        return Sql.newInstance(
            "jdbc:mysql://${DB_HOST}:${DB_PORT}/${DB_NAME}",
            DB_USER,
            DB_PASS,
            "com.mysql.jdbc.Driver"
        );
    }
}
