'use strict';

const path = require('path')
const fs = require('fs')
const request = require('request');
const config = require('./config/config.json');
const projects = require('./config/projects.json')
const outputPath = './';

const packageVersion = process.env.PACKAGE_VERSION;
const packageCommits = JSON.parse(process.env.PACKAGE_COMMITS);

main()

async function main() {
    let adocContent = buildMainTitle();
    for (var i = 0; i < projects.length; i++) {
        adocContent += await getProjectInfo(projects[i], packageCommits[projects[i].name]);
    }

    fs.writeFileSync(path.join(outputPath, `package-${packageVersion}.adoc`), adocContent);

    let htmlContent = convertAdocToHtml(adocContent);
    fs.writeFileSync(path.join(outputPath, `package-${packageVersion}.html`), htmlContent);

    await createPageConfluence(`Package version ${packageVersion}`, htmlContent);
};

async function getProjectInfo(project, projectInfo) {
    var result = [];

    var projectId = project.id;
    var projectName = project.name;

    let items = await getCommitsFromGitlab(projectId, projectInfo[0].commit_hash)

    console.log(`${projectName}-${projectInfo[1].version} \t${items.length} commits`);

    result.push(`\n== ${projectName} ${projectInfo[1].version}`);
    result.push(`\ncommit: ${projectInfo[0].commit_hash} \n`)
    var previousDate = '';
    for (var i = 0; i < items.length; i++) {
        if (items[i].message.toLowerCase().substr(0, 6) != 'chore:') {
            if (previousDate != items[i].date) {
                result.push(`${items[i].date}::`);
                previousDate = items[i].date;
            }
            result.push(`* \t${items[i].message} _(${items[i].author})_`);
        }
    }

    result.push('');
    result.push('');
    result.push('');

    return result.join('\n');
}

async function createPageConfluence(title, content) {
    return new Promise((resolve, reject) => {
        var url = `${config.confluence.server}/rest/api/content?spaceKey=VL&title=Release+Notes`;
        var result = '';
        var headers = {
            'Content-Type': 'application/json',
            'cache-control': 'no-cache',
            'Authorization': `Basic ${config.confluence.authentication}`,
            'Accept': '*/*'
        };
        var data = {
            type: 'page',
            title: title,
            space: { key: 'VL' },
            ancestors: [{ id: config.confluence.ancestor }],
            body: {
                storage: {
                    value: content,
                    representation: 'storage'
                }
            }
        };
        request.post(
            {
                uri: url,
                headers: headers,
                body: JSON.stringify(data)
            },
            (error, res, body) => {
                var data = JSON.parse(body);
                if (data.id & data.title) {
                    console.log(`  ${data.id}, ${data.title}`);
                } else {
                    console.log(`  ${data.statusCode}, ${data.message}`);
                }

                resolve(true);
            }
        );
    });
}


function buildMainTitle() {
    var result = [];
    result.push(`= Viridian Baneco package ${packageVersion}`);
    result.push(':toc2:');
    result.push(':icons:');
    result.push(':idprefix:');
    result.push(':idseparator: -');
    result.push(':sectanchors:');
    result.push(':source-highlighter: highlight.js');
    result.push('');
    result.push('');
    result.push('');

    return result.join('\n');
}

function convertAdocToHtml(content) {
    var asciidoctor = require('asciidoctor.js')();
    return asciidoctor.convert(content); // <2>
}

function getCommitsFromGitlab(projectId, refName) {
    return new Promise((resolve, reject) => {
        var url = `${config.git.server}/api/v4/projects/${projectId}/repository/commits?ref_name=${refName}&per_page=15`;

        var result = '';
        request.get(url, { headers: { 'PRIVATE-TOKEN': config.git.token } }, (error, res, body) => {
            if (error) {
                console.error(error);
                reject(error);
            }
            var data = JSON.parse(body);
            if (data != null && data.length != null && data.length > 0) {
                var commits = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].parent_ids && data[i].parent_ids.length == 1) {
                        commits.push({
                            date: data[i].authored_date ? data[i].authored_date.substr(0, 10) : '',
                            message: data[i].title ? data[i].title : '',
                            author: data[i].author_name ? data[i].author_name : ''
                        });
                    }
                }
                resolve(commits);
            } else {
                resolve([]);
            }
        });
    });
}