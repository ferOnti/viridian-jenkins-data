const { gitDescribeSync } = require('git-describe');
const { resolve } = require('path');
const { writeFileSync } = require('fs-extra');
const shell = require('shelljs');

const path = process.argv[2];
const rawBranch = process.argv[3];
const isDependency = process.argv[4] == 'true';

let branch = 'dev';

if (/^release\/.*$/.test(rawBranch)) branch = 'release';
else if (/^hotfix\/.*$/.test(rawBranch)) branch = 'hotfix';

shell.cd(path);
let commitCount = 0;


//if(branch == 'release')
//    commitCount = shell.exec('git rev-list --no-merges --count HEAD  ^origin/dev', { silent: true }).stdout;

//if(branch == 'hotfix')
//    commitCount = shell.exec('git rev-list --count HEAD  ^origin/dev', { silent: true }).stdout;

if (isDependency && branch == 'hotfix')
    commitCount = shell.exec('git rev-list --no-merges --count HEAD  ^origin/dev', { silent: true }).stdout;

if (branch == 'release') 
    commitCount = shell.exec('git rev-list --no-merges --count HEAD  ^origin/dev', { silent: true }).stdout;

if (branch == 'hotfix')
    if (isDependency) 
        commitCount = shell.exec('git rev-list --count HEAD  ^origin/dev', { silent: true }).stdout;
    else 
        commitCount = shell.exec('git rev-list --count HEAD  ^origin/dev', { silent: true }).stdout;

const gitInfo = gitDescribeSync(path, {
    dirtyMark: false,
    dirtySemver: false,
    customArguments: ['--abbrev=40'],
    match: '[0-9]*'
});

const file = resolve(path, 'git-describe.json');
writeFileSync(file, JSON.stringify(gitInfo, null, 4), { encoding: 'utf-8' });

let gitDescribe = require(`${path}/git-describe.json`);

let version = {};
switch (branch) {
    case 'dev':
        if (isDependency)
            version.semver = `${gitDescribe.semver.major}.${gitDescribe.semver.minor}.${gitDescribe.semver.patch + gitDescribe.distance}`;
        else 
            version.semver = `${gitDescribe.semver.major}.${gitDescribe.semver.minor}.${gitDescribe.semver.patch}-${gitDescribe.distance}`;
        version.forPackage = false;
        break;
    case 'release':
        version.semver = `${gitDescribe.semver.major}.${gitDescribe.semver.minor + 1}.${parseInt(commitCount)}`;
        version.forPackage = true;
        break;
    case 'hotfix':
        if (isDependency) 
            version.semver = `${gitDescribe.semver.major}.${gitDescribe.semver.minor + 1}.${parseInt(commitCount)}`;
        else 
            version.semver = `${gitDescribe.semver.major}.${gitDescribe.semver.minor}.${gitDescribe.semver.patch + parseInt(commitCount)}`;
        version.forPackage = true;
        break;
    default:
        version.semver = `${gitDescribe.semver.major}.${gitDescribe.semver.minor}.${gitDescribe.semver.patch}-${gitDescribe.distance}`;
        version.forPackage = false;
        break;
}

version.commitHash = gitDescribe.hash;
console.log(version);
