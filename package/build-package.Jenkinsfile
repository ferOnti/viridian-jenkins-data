@Library('viridian') _

node('linux') {
    def packageVersion = "1.${BUILD_NUMBER}" 

    def botPackage = "viridian-baneco-bot-${packageVersion}"
    def acPackage = "viridian-baneco-ac-${packageVersion}"

    try {
        slack.init()
        slack.setTitle("build-package")
    
        stage('BUILD DOCS') {
            sql.getPackageCommits()
            dir('output') {
                def docs = sh (
                    script: "PACKAGE_COMMITS='${env.package_commits}' PACKAGE_VERSION='${packageVersion}' node ${JENKINS_DATA}/node/docs.js",
                    returnStdout: true
                ).trim()
            }
        }

        stage('BUILD RPM') {
            //todo move this params to external file
            def botParams = [
                'environment':'bot',
                'nexus-server':'nexus.viridian.ltd',
                'spring-cloud-config-token': 'U2FsdGVkX18hR33QidD6tmyrQOZ06pnB+GZn7/XeN/wfkk7X1eTSVYldS/z2NTDq',
                'spring-cloud-config-username': 'U2FsdGVkX19DCz+2t2gfpqmlLuXd/PHe4NNiqXI4mSM=',
                'spring-cloud-config-password': 'U2FsdGVkX1+skPwt+VHRKNDzZTFTe8RMgWM50Uemue0=',
                'git-username': 'U2FsdGVkX1/8T+o1ZeiCTs5nu83KN9xt86KEM8apLN4=',
                'git-password': 'U2FsdGVkX18Sinl885jRkwCfxXh2lE72jGumCZa9LJU=',
                'vault-host': 'bot.viridian.ltd',
                'gitea-server': 'https://bot.viridian.ltd:3000/VIRIDIAN',
                'docker-network': 'viridian.ltd'
            ]

            def acParams = [
                'environment':'prod',
                'nexus-server':'nexusbaneco.viridian.ltd',
                'spring-cloud-config-token': 'U2FsdGVkX1+JUeE4G/MVGCHfRUf6v9+RYHqvYUTOglzDpre355VrSJ/yPliMOcVh',
                'spring-cloud-config-username': 'U2FsdGVkX1+qiJGuVPDyjnUIXPzvtvyAlgDqtyq/IUY=',
                'spring-cloud-config-password': 'U2FsdGVkX1/CPBmmWoEnf7Uir0JXHjYcgEyc9mUoYPY=',
                'git-username': 'U2FsdGVkX1+/x5bAzSY3oH2EazzTJRyjmO0JHrQjVdo=',
                'git-password': 'U2FsdGVkX1/d5qlyo//jHq/hPrjnfuWKnddeBSF7zMw=',
                'vault-host': 'docker02-vault.viridian.ltd',
                'gitea-server': 'https://web-gitea.viridian.ltd:3000/BANECO',
                'docker-network': 'baneco.com.bo'
            ]

            botParams << params.findAll{ it.value != '' }
            acParams << params.findAll{ it.value != '' }

            def botFinalParams = [:]
            botParams.each { param ->
                botFinalParams.put(param.key, param.value.replaceAll("/", "\\\\/").replaceAll("&", "\\\\&"))
            }

            def acFinalParams = [:]
            acParams.each { param ->
                acFinalParams.put(param.key, param.value.replaceAll("/", "\\\\/").replaceAll("&", "\\\\&"))
            }

            def zkVersion = botFinalParams.zookeeper_version

            dir('output') {
                println "Getting zookeeper dump"
                def zkDump = "dump-${packageVersion}.yml"
                sql.getZookeeperCommit(zkVersion)
                gitlab.getFile(153, 'dump.yml', env.zk_commit_hash, zkDump)

                println "Replacing values in zookeeper dump"
                sh "sed -i \"s/ymlVersion: \\\"0.0.0\\\"/ymlVersion: \\\"${zkVersion}\\\"/g\" ${zkDump}"
                sh "sed -i \"s/releaseVersion: \\\"0.0\\\"/releaseVersion: \\\"${packageVersion}\\\"/g\" ${zkDump}"
                
                dir("sources/${botPackage}") {
                    println "Copying sources from ${JENKINS_DATA}/package/sources"
                    sh "cp -r ${JENKINS_DATA}/package/sources/. ."

                    println "Replacing params in sources"
                    botFinalParams.each { param ->
                        sh "find . -type f -name '*.yml' -exec sed -i \"s/{${param.key}}/${param.value}/g\" {} +"
                    }

                    dir('zookeeper') {
                        sh "cp ../../../${zkDump} ."
                    }

                    dir('docs') {      
                        println "Copying adoc and html files"
                        sh "cp ../../../package-${packageVersion}.adoc ."
                        sh "cp ../../../package-${packageVersion}.html ."
                    }
                }
               
                dir("sources/${acPackage}") {
                    println "Copying sources from ${JENKINS_DATA}/package/sources"
                    sh "cp -r ${JENKINS_DATA}/package/sources/. ."

                    println "Replacing params in sources"
                    acFinalParams.each { param ->
                        sh "find . -type f -name '*.yml' -exec sed -i \"s/{${param.key}}/${param.value}/g\" {} +"
                    }

                    dir('zookeeper') {
                        sh "cp ../../../${zkDump} ."
                    }

                    dir('docs') {      
                        println "Copying adoc and html files"
                        sh "cp ../../../package-${packageVersion}.adoc ."
                        sh "cp ../../../package-${packageVersion}.html ."
                    }
                }

                dir("specs"){
                    println "Copying specs from ${JENKINS_DATA}/package/specs/viridian-baneco-bot.spec to ${botPackage}.spec"
                    sh "cp ${JENKINS_DATA}/package/specs/viridian-baneco-bot.spec ${botPackage}.spec"
                    println "Replacing values in spec file"
                    sh "sed -i \"s/{packageVersion}/${packageVersion}/g\" ${botPackage}.spec"
            
                    println "Copying specs from ${JENKINS_DATA}/package/specs/viridian-baneco-ac.spec to ${acPackage}.spec"
                    sh "cp ${JENKINS_DATA}/package/specs/viridian-baneco-ac.spec ${acPackage}.spec"
                    println "Replacing values in spec file"
                    sh "sed -i \"s/{packageVersion}/${packageVersion}/g\" ${acPackage}.spec"
                }

                dir("sources") {
                    println "Tar sources for ac"
                    sh "tar -zcvf ${acPackage}.tar.gz ${acPackage}"
                
                    println "Tar sources for bot"
                    sh "tar -zcvf ${botPackage}.tar.gz ${botPackage}"
                }
                
                println "Copying spec file from specs/${botPackage}.spec to ~/rpmbuild/SPECS/${botPackage}.spec"
                sh "cp specs/${botPackage}.spec ~/rpmbuild/SPECS/${botPackage}.spec"

                println "Copying spec file from specs/${acPackage}.spec to ~/rpmbuild/SPECS/${acPackage}.spec"
                sh "cp specs/${acPackage}.spec ~/rpmbuild/SPECS/${acPackage}.spec"
                
                println "Copying tar file from ${botPackage}.tar.gz to ~/rpmbuild/SOURCES/${botPackage}.tar.gz"
                sh "cp sources/${botPackage}.tar.gz ~/rpmbuild/SOURCES/${botPackage}.tar.gz"

                println "Copying tar file from ${acPackage}.tar.gz to ~/rpmbuild/SOURCES/${acPackage}.tar.gz"
                sh "cp sources/${acPackage}.tar.gz ~/rpmbuild/SOURCES/${acPackage}.tar.gz"
            }

            println "Building rpm for bot"
            sh "rpmbuild --target=noarch-redhat-linux -bb ~/rpmbuild/SPECS/${botPackage}.spec"
        
            println "Uploading rpm for bot to nexus"
            sh "curl -v -u admin:admin123 --upload-file ~/rpmbuild/RPMS/noarch/${botPackage}-1.noarch.rpm http://nexus.viridian.ltd/repository/rpms/packages/viridian-baneco-bot/${botPackage}-1.noarch.rpm"

            println "Building rpm for ac"
            sh "rpmbuild --target=noarch-redhat-linux -bb ~/rpmbuild/SPECS/${acPackage}.spec"
        
            println "Uploading rpm for ac to nexus"
            sh "curl -v -u admin:admin123 --upload-file ~/rpmbuild/RPMS/noarch/${acPackage}-1.noarch.rpm http://nexus.viridian.ltd/repository/rpms/packages/viridian-baneco-ac/${acPackage}-1.noarch.rpm"

            sql.savePackage(packageVersion, botFinalParams)
        }

        stage('EXEC RPM IN BOT') {
            sh "scp ~/rpmbuild/RPMS/noarch/${botPackage}-1.noarch.rpm bot.viridian.ltd:/home/jenkins/rpms"
            sh "scp ~/rpmbuild/RPMS/noarch/${acPackage}-1.noarch.rpm bot.viridian.ltd:/home/jenkins/rpms"
            sh "ssh bot.viridian.ltd -t 'sudo rpm -ivh /home/jenkins/rpms/${botPackage}-1.noarch.rpm --force'"
        }

        currentBuild.result = "SUCCESS"
        slack.setSubTitle("Packages created: \n ${botPackage} \n ${acPackage}")
    } catch(Exception ex) {
        println ex
        currentBuild.result = "FAILED"
        slack.setSubTitle("There was an error during package build")
    } finally {
        slack.message()
        deleteDir()
    }
}

