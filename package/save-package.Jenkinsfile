@Library('viridian') _
import groovy.json.JsonSlurperClassic

node('linux') {
    def projectsForMerge = []
    try {
        slack.init()
        slack.setTitle("save-package")

        sql.getPackageInfo()
        println "Package version: ${env.package_version}"
        println "Package status: ${env.package_status}"
        println "Package detail: ${env.package_detail}"
        println "Package date: ${env.package_date}"

        def versions = new JsonSlurperClassic().parseText(env.package_detail)

        /* stage('INSPECTOR-API') {
            env.project_version = versions.inspector_api_version
            env.project_name = 'Viridian.Inspector.Api'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/devtools/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        } */
 
        stage('MANAGER-API') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.manager_api_version
            env.project_name = 'Viridian.Manager.Api'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/manager/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('MANAGER-HOSTED') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.manager_hosted_version
            env.project_name = 'Viridian.Manager.Hosted'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/manager/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('ACCOUNTS-API') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.accounts_api_version
            env.project_name = 'Viridian.Accounts.Api'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/acc/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('ACCOUNTS-HOSTED') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.accounts_hosted_version
            env.project_name = 'Viridian.Accounts.Hosted'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/acc/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('SECURITY-API') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.security_api_version
            env.project_name = 'Viridian.Security.Api'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/sec/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('BENEFICIARIES-API') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.beneficiaries_api_version
            env.project_name = 'Viridian.Beneficiaries.Api'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/beneficiaries/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('BENEFICIARIES-HOSTED') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.beneficiaries_hosted_version
            env.project_name = 'Viridian.Beneficiaries.Hosted'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/beneficiaries/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('TRANSACTIONS-API') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.transactions_api_version
            env.project_name = 'Viridian.Transactions.Api'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/transactions/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('TRANSACTIONS-HOSTED') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.transactions_hosted_version
            env.project_name = 'Viridian.Transactions.Hosted'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/transactions/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('UTILITIES-API') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.utilities_api_version
            env.project_name = 'Viridian.Utilities.Api'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/utilities/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('UTILITIES-HOSTED') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.utilities_hosted_version
            env.project_name = 'Viridian.Utilities.Hosted'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/utilities/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('SIMPLE-API') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.simple_api_version
            env.project_name = 'Viridian.Simple.Api'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/simple/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('SIMPLE-CERTSMANAGER') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.simple_certsmanager_version
            env.project_name = 'Viridian.Simple.CertsManager'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/simple/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('SERVICEPAYMENTS-API') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.servicepayments_api_version
            env.project_name = 'Viridian.ServicePayments.Api'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/service-payments/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('NOTIFICATIONS-API') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.notifications_api_version
            env.project_name = 'Viridian.Notifications.Api'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/notifications/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('NOTIFICATIONS-HOSTED') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.notifications_hosted_version
            env.project_name = 'Viridian.Notifications.Hosted'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/notifications/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('SERVER-CONFIG') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.server_config_version
            env.project_name = 'server-config'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/config/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('SERVER-EUREKA') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.server_eureka_version
            env.project_name = 'server-eureka'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/config/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('SERVER-GATEWAY') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.server_gateway_version
            env.project_name = 'server-gateway'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/gat/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('SERVER-GATEWAY-BANK') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.server_gateway_bank_version
            env.project_name = 'server-gateway-bank'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/gat/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        sh "git merge origin/${env.project_branch}"
                        sh "git push"
                        sh "git push --delete origin ${env.project_branch}"
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        stage('ZOOKEEPER') {
            slackStage = env.STAGE_NAME

            env.project_version = versions.zookeeper_version
            env.project_name = 'Viridian.Zookeeper.Config'
            sql.getProjectVersionInfo()

            if(!env.project_in_production.toBoolean()) {
                sh "git clone git@gitlab.viridian.ltd:vdb/config/${env.project_name}.git"
                dir(env.project_name) {
                    sh "git tag -a ${env.project_version} ${env.project_commit_hash} -m '${env.project_version}'"
                    sh "git push --tags"
                    sql.updateProjectInfo()
                    try {
                        if(env.project_branch != 'dev') {
                            sh "git merge origin/${env.project_branch}"
                            sh "git push"
                            sh "git push --delete origin ${env.project_branch}"
                        }
                    }
                    catch (Exception ex) {
                        projectsForMerge << "${env.project_name} (${env.project_branch} > dev)" 
                    }
                }
            } else {
                println "Project ${env.project_name} with version ${env.project_version} already in production"
            }
        }

        sql.updatePackageInfo()

        def msg = ''
        if(!projectsForMerge.isEmpty()) {
            msg = "The following projects need manual merge: \n" + projectsForMerge.join('\n')
        } else {
            msg = "Package with version: ${env.package_version} was saved"
        }

        currentBuild.result = "SUCCESS"
        slack.setSubTitle(msg)
    } catch(Exception ex) {
        println ex
        currentBuild.result = "FAILED"
        slack.setSubTitle("There was an error during package save")
    } finally {
        slack.message()
        deleteDir()
    }
}

