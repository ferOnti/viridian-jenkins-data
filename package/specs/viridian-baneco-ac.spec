Name:   viridian-baneco-ac
Version:        {packageVersion}
Release:        1
Summary:        minimal spec
License:        AGPL
Source:  viridian-baneco-ac-{packageVersion}.tar.gz
Requires: tree

%description
bar


%prep
#rm -rf Viridian*
echo "BUILDROOT = $RPM_BUILD_ROOT"
pwd
mkdir -p $RPM_BUILD_ROOT/opt/viridian

%setup

%pre

echo "pre-install viridian-baneco.rpm"
cp -R /opt/viridian/ /opt/viridian-backup/{packageVersion}/

%post
echo ""
echo "post-install viridian-baneco.rpm"
tree -L 2 /opt/viridian
tree -L 2 /opt/viridian-backup

%build

%install

echo "installing"
mkdir -p %{buildroot}/opt/viridian
cp -a * %{buildroot}/opt/viridian
mkdir -p %{buildroot}/opt/viridian-backup

%files
/opt/viridian-backup
/opt/viridian/config/*
/opt/viridian/gitea/*
/opt/viridian/docs/*
/opt/viridian/vault/*
/opt/viridian/vdb/*
/opt/viridian/zookeeper/*

%changelog
