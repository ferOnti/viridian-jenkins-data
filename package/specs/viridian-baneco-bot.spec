Name:   viridian-baneco-bot
Version:        {packageVersion}
Release:        1
Summary:        minimal spec
License:        AGPL
Source:  viridian-baneco-bot-{packageVersion}.tar.gz
Requires: tree

%description
bar


%prep
#rm -rf Viridian*
echo "BUILDROOT = $RPM_BUILD_ROOT"
pwd
mkdir -p $RPM_BUILD_ROOT/opt/viridian

%setup

%pre

echo "pre-install viridian-baneco.rpm"
cp -R /opt/viridian/ /opt/viridian-backup/{packageVersion}/

%post
echo ""
echo "post-install viridian-baneco.rpm"
tree -L 2 /opt/viridian

echo "\nready to update config microservices"
cd /opt/viridian/config
cat /opt/viridian/config/docker-compose.yml | grep image | awk '{print $2 }'
echo "\nrestart config microservices"
docker-compose up -d --remove-orphans

echo "\nready to update viridian microservices"
cd /opt/viridian/vdb
cat /opt/viridian/vdb/docker-compose.yml | grep image | awk '{print $2 }'
echo "\nrestart viridian microservices"
docker-compose up -d --remove-orphans

echo "upload zookeeper dump version"
cd /opt/viridian/zookeeper
cat dump-{packageVersion}.yml | grep ymlVersion

%build

%install

echo "installing"
mkdir -p %{buildroot}/opt/viridian
cp -a * %{buildroot}/opt/viridian
mkdir -p %{buildroot}/opt/viridian-backup

%files
/opt/viridian-backup
/opt/viridian/config/*
/opt/viridian/gitea/*
/opt/viridian/docs/*
/opt/viridian/vault/*
/opt/viridian/vdb/*
/opt/viridian/zookeeper/*

%changelog
