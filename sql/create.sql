CREATE TABLE IF NOT EXISTS environments (
    id int NOT NULL AUTO_INCREMENT,
    env varchar(255) NOT NULL,
    enabled tinyint(1) NOT NULL,
    PRIMARY KEY (id)
); 

CREATE TABLE IF NOT EXISTS project_versions (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    version VARCHAR(255) NOT NULL,
    commit_hash VARCHAR(255) NOT NULL,
    is_dependency tinyint(1) NOT NULL,
    branch VARCHAR(255) NOT NULL,
    for_package tinyint(1) NOT NULL DEFAULT 0,
    in_production tinyint(1) NOT NULL DEFAULT 0,
    date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    build_url VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS packages (
    id int NOT NULL AUTO_INCREMENT,
    status VARCHAR(50) NOT NULL,
    version VARCHAR(10) NOT NULL,
    detail TEXT NOT NULL,
    date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS projects (
    id int NOT NULL,
    name VARCHAR(255) NOT NULL,
    repo VARCHAR(255) NOT NULL,
    gitlab_url VARCHAR(255) NOT NULL,
    is_deployable tinyint(1) NOT NULL DEFAULT 0,
    has_tests tinyint(1) NOT NULL DEFAULT 0,
    docker_image varchar(255),
    container_name varchar(255),
    port varchar(255),
    playbook_type varchar(255),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS test_queue (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    version VARCHAR(255) NOT NULL,
    date_created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    date_executed DATETIME,
    job_url VARCHAR(255),
    status VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (63, 'Viridian.Common','git@gitlab.viridian.ltd:vdb/common/Viridian.Common.git', 'http://gitlab.viridian.ltd/vdb/common/Viridian.Common', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (222, 'Viridian.Common.Demo','git@gitlab.viridian.ltd:vdb/common/Viridian.Common.Demo.git', 'http://gitlab.viridian.ltd/vdb/common/Viridian.Common.Demo', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (64, 'Viridian.Common.Baneco','git@gitlab.viridian.ltd:vdb/common/Viridian.Common.Baneco.git', 'http://gitlab.viridian.ltd/vdb/common/Viridian.Common.Baneco', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (223, 'Viridian.Manager.Common','git@gitlab.viridian.ltd:vdb/manager/Viridian.Manager.Common.git', 'http://gitlab.viridian.ltd/vdb/manager/Viridian.Manager.Common', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (167, 'Viridian.Manager.Api','git@gitlab.viridian.ltd:vdb/manager/Viridian.Manager.Api.git', 'http://gitlab.viridian.ltd/vdb/manager/Viridian.Manager.Api', 1, 1, 'viridian-manager-api','manager-api','7100', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (224, 'Viridian.Manager.Hosted','git@gitlab.viridian.ltd:vdb/manager/Viridian.Manager.Hosted.git', 'http://gitlab.viridian.ltd/vdb/manager/Viridian.Manager.Hosted', 1, 0, 'viridian-manager-hosted','manager-hosted','7101', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (206, 'Viridian.Security.Common','git@gitlab.viridian.ltd:vdb/sec/Viridian.Security.Common.git', 'http://gitlab.viridian.ltd/vdb/sec/Viridian.Security.Common', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (66, 'Viridian.Security.Api','git@gitlab.viridian.ltd:vdb/sec/Viridian.Security.Api.git', 'http://gitlab.viridian.ltd/vdb/sec/Viridian.Security.Api', 1, 1, 'viridian-security-api','security-api','7020', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (207, 'Viridian.Security.Hosted','git@gitlab.viridian.ltd:vdb/sec/Viridian.Security.Hosted.git', 'http://gitlab.viridian.ltd/vdb/sec/Viridian.Security.Hosted', 1, 0, 'viridian-security-hosted','security-hosted','7021', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (209, 'Viridian.Accounts.Common','git@gitlab.viridian.ltd:vdb/acc/Viridian.Accounts.Common.git', 'http://gitlab.viridian.ltd/vdb/acc/Viridian.Accounts.Common', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (67, 'Viridian.Accounts.Api','git@gitlab.viridian.ltd:vdb/acc/Viridian.Accounts.Api.git', 'http://gitlab.viridian.ltd/vdb/acc/Viridian.Accounts.Api', 1, 1, 'viridian-accounts-api','accounts-api','7030', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (202, 'Viridian.Accounts.Hosted','git@gitlab.viridian.ltd:vdb/acc/Viridian.Accounts.Hosted.git', 'http://gitlab.viridian.ltd/vdb/acc/Viridian.Accounts.Hosted', 1, 0, 'viridian-accounts-hosted','accounts-hosted','7031', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (210, 'Viridian.Beneficiaries.Common','git@gitlab.viridian.ltd:vdb/beneficiaries/Viridian.Beneficiaries.Common.git', 'http://gitlab.viridian.ltd/vdb/beneficiaries/Viridian.Beneficiaries.Common', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (123, 'Viridian.Beneficiaries.Api','git@gitlab.viridian.ltd:vdb/beneficiaries/Viridian.Beneficiaries.Api.git', 'http://gitlab.viridian.ltd/vdb/beneficiaries/Viridian.Beneficiaries.Api', 1, 1, 'viridian-beneficiaries-api','beneficiaries-api','7040', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (211, 'Viridian.Beneficiaries.Hosted','git@gitlab.viridian.ltd:vdb/beneficiaries/Viridian.Beneficiaries.Hosted.git', 'http://gitlab.viridian.ltd/vdb/beneficiaries/Viridian.Beneficiaries.Hosted', 1, 0, 'viridian-beneficiaries-hosted','beneficiaries-hosted','7041', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (213, 'Viridian.Transactions.Common','git@gitlab.viridian.ltd:vdb/transactions/Viridian.Transactions.Common.git', 'http://gitlab.viridian.ltd/vdb/transactions/Viridian.Transactions.Common', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (150, 'Viridian.Transactions.Api','git@gitlab.viridian.ltd:vdb/transactions/Viridian.Transactions.Api.git', 'http://gitlab.viridian.ltd/vdb/transactions/Viridian.Transactions.Api', 1, 1, 'viridian-transactions-api','transactions-api','7070', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (196, 'Viridian.Transactions.Hosted','git@gitlab.viridian.ltd:vdb/transactions/Viridian.Transactions.Hosted.git', 'http://gitlab.viridian.ltd/vdb/transactions/Viridian.Transactions.Hosted', 1, 0, 'viridian-transactions-hosted','transactions-hosted','7071', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (214, 'Viridian.Utilities.Common','git@gitlab.viridian.ltd:vdb/utilities/Viridian.Utilities.Common.git', 'http://gitlab.viridian.ltd/vdb/utilities/Viridian.Utilities.Common', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (179, 'Viridian.Utilities.Api','git@gitlab.viridian.ltd:vdb/utilities/Viridian.Utilities.Api.git', 'http://gitlab.viridian.ltd/vdb/utilities/Viridian.Utilities.Api', 1, 1, 'viridian-utilities-api','utilities-api','7090', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (200, 'Viridian.Utilities.Hosted','git@gitlab.viridian.ltd:vdb/utilities/Viridian.Utilities.Hosted.git', 'http://gitlab.viridian.ltd/vdb/utilities/Viridian.Utilities.Hosted', 1, 0, 'viridian-utilities-hosted','utilities-hosted','7091', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (217, 'Viridian.Simple.Common','git@gitlab.viridian.ltd:vdb/simple/Viridian.Simple.Common.git', 'http://gitlab.viridian.ltd/vdb/simple/Viridian.Simple.Common', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (188, 'Viridian.Simple.Api','git@gitlab.viridian.ltd:vdb/simple/Viridian.Simple.Api.git', 'http://gitlab.viridian.ltd/vdb/simple/Viridian.Simple.Api', 1, 1, 'viridian-simple-api','simple-api','7050', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (237, 'Viridian.Simple.Hosted','git@gitlab.viridian.ltd:vdb/simple/Viridian.Simple.Hosted.git', 'http://gitlab.viridian.ltd/vdb/simple/Viridian.Simple.Hosted', 1, 0, 'viridian-simple-hosted','simple-hosted','7052', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (154, 'Viridian.Simple.CertsManager','git@gitlab.viridian.ltd:vdb/simple/Viridian.Simple.CertsManager.git', 'http://gitlab.viridian.ltd/vdb/simple/Viridian.Simple.CertsManager', 1, 1, 'viridian-simple-certsmanager','simple-certsmanager','7051', 'java');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (219, 'Viridian.ServicePayments.Common','git@gitlab.viridian.ltd:vdb/service-payments/Viridian.ServicePayments.Common.git', 'http://gitlab.viridian.ltd/vdb/service-payments/Viridian.ServicePayments.Common', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (141, 'Viridian.ServicePayments.Api','git@gitlab.viridian.ltd:vdb/service-payments/Viridian.ServicePayments.Api.git', 'http://gitlab.viridian.ltd/vdb/service-payments/Viridian.ServicePayments.Api', 1, 1, 'viridian-servicepayments-api','servicepayments-api','7060', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (218, 'Viridian.Notifications.Common','git@gitlab.viridian.ltd:vdb/notifications/Viridian.Notifications.Common.git', 'http://gitlab.viridian.ltd/vdb/notifications/Viridian.Notifications.Common', 0, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (232, 'Viridian.Notifications.Api','git@gitlab.viridian.ltd:vdb/notifications/Viridian.Notifications.Api.git', 'http://gitlab.viridian.ltd/vdb/notifications/Viridian.Notifications.Api', 1, 1, 'viridian-notifications-api','notifications-api','7080', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (198, 'Viridian.Notifications.Hosted','git@gitlab.viridian.ltd:vdb/notifications/Viridian.Notifications.Hosted.git', 'http://gitlab.viridian.ltd/vdb/notifications/Viridian.Notifications.Hosted', 1, 0, 'viridian-notifications-hosted','notifications-hosted','7081', 'netcore');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (47, 'server-config','git@gitlab.viridian.ltd:vdb/config/server-config.git', 'http://gitlab.viridian.ltd/vdb/config/server-config', 1, 1, 'viridian-server-config','server-config','7000', 'server-config');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (116, 'server-eureka','git@gitlab.viridian.ltd:vdb/config/server-eureka.git', 'http://gitlab.viridian.ltd/vdb/config/server-eureka', 1, 0, 'viridian-server-eureka','server-eureka','7005', 'server-eureka');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (104, 'server-gateway','git@gitlab.viridian.ltd:vdb/gat/server-gateway.git', 'http://gitlab.viridian.ltd/vdb/gat/server-gateway', 1, 0, 'viridian-server-gateway','server-gateway','443', 'java');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (148, 'server-gateway-bank','git@gitlab.viridian.ltd:vdb/gat/server-gateway-bank.git', 'http://gitlab.viridian.ltd/vdb/gat/server-gateway-bank', 1, 0, 'viridian-server-gateway-bank','server-gateway-bank','7011', 'java');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (176, 'server-gateway-baneco','git@gitlab.viridian.ltd:vdb/gat/server-gateway-baneco.git', 'http://gitlab.viridian.ltd/vdb/gat/server-gateway-baneco', 1, 0, 'viridian-server-gateway-baneco','server-gateway-baneco','7011', 'java');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (153, 'Viridian.Zookeeper.Config','git@gitlab.viridian.ltd:vdb/config/Viridian.Zookeeper.Config.git', 'http://gitlab.viridian.ltd/vdb/config/Viridian.Zookeeper.Config', 1, 0, null, null, null, null);

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (58, 'green_bank','git@gitlab.viridian.ltd:gb/green_bank.git', 'http://gitlab.viridian.ltd/gb/green_bank', 1, 1, 'viridian-greenbank','greenbank','8091', 'java');

INSERT INTO projects (id,name,repo, gitlab_url, is_deployable, has_tests, docker_image, container_name, port, playbook_type) 
VALUES (183, 'Viridian.Inspector.Api','git@gitlab.viridian.ltd:vdb/devtools/Viridian.Inspector.Api.git', 'http://gitlab.viridian.ltd/vdb/devtools/Viridian.Inspector.Api.git', 1, 1, 'viridian-inspector-api','inspector-api','7200', 'netcore');

INSERT INTO environments (env,enabled) 
VALUES ('dev',1);

INSERT INTO environments (env,enabled) 
VALUES ('testing',1);

INSERT INTO environments (env,enabled) 
VALUES ('master',1);