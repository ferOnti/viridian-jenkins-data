#!/bin/bash

registry='nexus.viridian.ltd'
auth='-u admin:admin123'
name=$1
tag=$2

digest=$(curl -v -s -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
            -X GET http://$registry/repository/docker-registry/v2/$name/manifests/$tag 2>&1 \
            | grep Docker-Content-Digest | awk '{print ($3)}')

digest=${digest//[$'\t\r\n']}

echo "Digest: $digest"

result=$(curl $auth -v -s -w "%{http_code}" -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
        -X DELETE http://$registry/repository/docker-registry/v2/$name/manifests/$digest -o /dev/null)

if [ $result -eq 202 ]; 
    then
        echo "Successfully deleted"
else 
    echo "Failed to delete"
fi

exit 0
