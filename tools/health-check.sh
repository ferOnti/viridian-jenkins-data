#!/bin/bash
url=$1
attempts=${2-11}
timeout=${3-5}
online=false

echo "Health check of $url."

for (( i=1; i<=$attempts; i++ ))
do
  code=`curl -sL --connect-timeout 20 --max-time 30 -w "%{http_code}\\n" "$url" -o /dev/null`

  echo "Found code $code for $url."

  if [ "$code" = "200" ]; then
    online=true
    break
  else
    echo "Service $url seems to be unhealthy. Waiting $timeout seconds."
    sleep $timeout
  fi
done

if $online; then
  echo "Service is healthy"
  exit 0
else
  echo "Service is unhealthy"
  exit 1
fi
